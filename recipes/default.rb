#
# Cookbook Name:: ruby_build_ruby
# Recipe:: default
#
# Copyright (C) 2015 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

node['ruby_build_rubies'].each do |version, attributes|
  ruby_build_ruby version do
    attributes.each do |attribute, value|
      send(attribute, value)
    end
  end
end
